# Symfony 1.4
#
# VERSION	1.0

# use the ubuntu base image provided by dotCloud
FROM ubuntu:22.04

# Environment variables
ENV ENVIRONMENT DEV
ENV DEBIAN_FRONTEND=noninteractive

# make sure the package repository is up to date
RUN apt update
RUN apt install -y software-properties-common
RUN apt install -y ca-certificates
RUN apt install -y unzip
RUN apt install -y curl
RUN apt install -y inetutils-ping
RUN apt install -y telnet
# # install editor tool
RUN apt -y install less
RUN apt -y install vim
RUN apt -y install joe
RUN apt -y install nano

RUN add-apt-repository ppa:ondrej/php
RUN apt update

# # install php
RUN apt -y install php7.4
RUN apt -y install php7.4-mbstring
RUN apt -y install php7.4-mysql
RUN apt -y install php7.4-opcache
RUN apt -y install php7.4-readline
RUN apt -y install php7.4-zip
RUN apt -y install php7.4-soap
RUN apt -y install php7.4-json
RUN apt -y install php7.4-xml
RUN apt -y install php7.4-curl
RUN apt -y install php7.4-gd
RUN apt -y install php7.4-fpm
RUN apt -y install php7.4-common
RUN apt -y install php7.4-cli
RUN apt -y install php7.4-stomp
RUN apt -y install php7.4-xdebug

# Install composer
RUN curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php
RUN php -r "if (hash_file('SHA384', '/tmp/composer-setup.php') == curl_exec(curl_init('https://composer.github.io/installer.sig'))) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('/tmp/composer-setup.php'); } echo PHP_EOL;"
RUN php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer

RUN /etc/init.d/php7.4-fpm start

# customize php7.4
ADD config/php7.4/fpm/php-fpm.conf /etc/php/7.4/fpm/php-fpm.conf
ADD config/php7.4/fpm/php.ini /etc/php/7.4/fpm/php.ini
ADD config/php7.4/fpm/pool.d/www.conf /etc/php/7.4/fpm/pool.d/www.conf
ADD config/php7.4/cli/php.ini /etc/php/7.4/cli/php.ini

RUN /etc/init.d/php7.4-fpm restart

# expose php-fpm port
EXPOSE 9000

# Custom scripts
ADD config/usr/local/bin/core-entrypoint.sh /usr/local/bin/core-entrypoint.sh
RUN chmod 777 /usr/local/bin/core-entrypoint.sh

ENTRYPOINT core-entrypoint.sh
